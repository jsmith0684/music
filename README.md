# Music

This CLI application acts as a batch processor. It accepts as arguments paths to a source file, changes file, and a destination for the output. The source file will have changes applied and the result is written to the destination path. See the usage section of this document for argument order. See the mixtapes.json and changes.json files in this repository for expected formats.

## Installation

Ruby 2.1.x or newer is required to run this code. Ruby installations should come with the Bundler gem.

Execute:

    $ bundle

## Usage

	$ bin/Music source.json changes.json output.json

source.json is a json file containing lists of user objects, playlist objects, and song objects.

changes.json is a json file containing lists of create, update, and delete actions for playlist objects.

output.json will be a newly created json file after applying all mutations in changes.json to source.json. **If the output.json file already exists, it will be overwritten.**

NOTE 1: Currently, JSON is the only supported format for input and output.

NOTE 2: Any error that occurs with input will cause the program to exit without writing output to disk.

## Schema

All mutations to the repository are expected to conform to the following constraints. Any violation of these constraints results in an error occurring, which prevents the output file from being written to disk.

Playlist properties:

- id: nonempty string
- user_id: nonempty string corresponding to an existing id in the users table
- songs_ids: array containing 0 or more string ids corresponding to existing ids in the songs table

Song properties:

- id: nonempty string
- artist: nonempty string
- title: nonempty string

User properties:

- id: nonempty string
- name: nonempty string

## Mutations

Mutations are only supported for playlist objects. Supported actions include create, delete, and update. Multiple objects can exist under each action.

NOTE: The update action acts as an upsert whereby it creates or updates an existing playlist object. During an update action, the current playlist object is replaced by the new playlist object contained in the changes file. Individual playlist properties are not patched/updated in place!

The changes file is expected to conform to the following format (required properties displayed):

``` json
	{
		"playlists": {
			"update": [
				{
					"id": "2",
					"user_id": "1",
					"song_ids": ["4", "5"]
				}
			],
			"delete": [
				{
					"id": "1"
				}
			],
			"create": [
				{
					"id": "3",
					"user_id": "2",
					"song_ids": ["1", "2"]
				}
			]
		}
	}
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

## Contributing

Bug reports and pull requests are welcome on Bitbucket at [Music](https://bitbucket.org/jsmith0684/Music/src).
