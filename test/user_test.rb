require_relative 'test_helper'
require_relative '../lib/Music/model/user'

describe 'User' do
  before do
    @user = Music::User.new(id: '1', name: 'bob dole')
    @user_json = '{"id": "2", "name": "john doe"}'
  end

  def test_user_class_exists
    assert Music::User.class
  end

  def test_user_properties
    assert_equal('1', @user.id)
    assert_equal('bob dole', @user.name)
  end

  def test_user_to_json
    u = @user.to_json()
    assert_kind_of(String, u)
  end
end
