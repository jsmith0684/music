require_relative 'test_helper'
require_relative '../lib/Music/validator/validators'

describe 'Validators' do

  def test_validators_class_exists
    assert Validators.class
  end

  def test_valid_string_valid
    h = {:name => 'bob'}
    assert(Validators.valid_string?(h, :name))
  end

  def test_valid_string_invalid
    h_empty = {}
    h_type = {:name => []}
    h_empty_string = {:name => ''}
    refute(Validators.valid_string?(h_empty, :name))
    refute(Validators.valid_string?(h_type, :name))
    refute(Validators.valid_string?(h_empty_string, :name))
  end

  def test_valid_array_valid
    h = {:songs => []}
    assert(Validators.valid_array?(h, :songs))
  end

  def test_valid_array_invalid
    h = {}
    h_type = {:songs => :you}
    refute(Validators.valid_array?(h, :songs))
    refute(Validators.valid_array?(h_type, :songs))
  end
end
