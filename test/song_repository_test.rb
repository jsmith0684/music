require_relative 'test_helper'
require_relative '../lib/Music/repository/song_repository'

class Validator
  def self.validate(playlist:, result:true)
    result
  end
end
class Song
  def initialize(obj)
  end
end

describe 'SongRepository' do

  def test_songrepository_class_exists
    assert Music::SongRepository.class
  end

  def test_model_class
    assert_equal(:song, Music::SongRepository.model())
  end

  def test_save_exit_invalid_song
    song = {:id => '1'}
    mock = MiniTest::Mock.new()
    mock.expect(:validate, false, [Hash])
    original = $stdout.clone()
    $stdout.reopen(File.new('/dev/null', 'w'))
    Validator.stub(:validate, mock) do
      assert_raises SystemExit do
        Music::SongRepository.save(song: song, model: Song, validator: mock)
      end
    end
    $stdout.reopen(original)
    mock.verify
  end

  def test_save_valid_song
    song = {:id => '1'}
    mock = MiniTest::Mock.new()
    mock.expect(:validate, true, [Hash])
    mock.expect(:call, nil, [Hash])
    Music::Repository.instance.stub(:save, mock) do
      assert_nil(Music::SongRepository.save(song: song, model: Song, validator: mock))
    end
    mock.verify
  end
end
