require_relative 'test_helper'
require_relative '../lib/Music/utility/json_formatter'

describe 'JSONFormatter' do

  before do
    @content = {:key1 => 'value1', :key2 => 'value2'}
    @content_json = '{"key1":"value1","key2":"value2"}'
    @formatter = JSONFormatter.new()
  end

  def test_jsonformatter_class_exists
    assert(JSONFormatter.class)
  end

  def test_format_valid_json
    assert(@formatter.format(content: @content))
  end

  def test_parse_valid_json
    assert(@formatter.parse(content: @content_json))
    assert_equal(@content, @formatter.parse(content: @content_json))
  end

  def test_parse_invalid_json
    invalid = '{"key1":"value1","key2":"value2"'
    original = $stdout.clone()
    $stdout.reopen(File.new('/dev/null', 'w'))
    assert_raises SystemExit do
      @formatter.parse(content: invalid)
    end
    $stdout.reopen(original)
  end
end
