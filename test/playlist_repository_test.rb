require_relative 'test_helper'
require_relative '../lib/Music/repository/playlist_repository'

class Validator
  def self.validate(playlist:, result:true)
    result
  end
end
class Playlist
  def initialize(obj)
  end
end

describe 'PlaylistRepository' do

  def test_class_exists
    assert Music::PlaylistRepository.class
  end

  def test_save_exit_invalid_playlist
    playlist = {:id => '1'}
    mock = MiniTest::Mock.new()
    mock.expect(:validate, false, [Hash])
    original = $stdout.clone()
    $stdout.reopen(File.new('/dev/null', 'w'))
    Validator.stub(:validate, mock) do
      assert_raises SystemExit do
        Music::PlaylistRepository.save(playlist:playlist, model: Playlist, validator: mock)
      end
    end
    $stdout.reopen(original)
    mock.verify
  end

  def test_save_valid_playlist
    playlist = {:id => '1'}
    mock = MiniTest::Mock.new()
    mock.expect(:validate, true, [Hash])
    mock.expect(:call, nil, [Hash])
    Music::Repository.instance.stub(:save, mock) do
      assert_nil(Music::PlaylistRepository.save(playlist:playlist, model: Playlist, validator: mock))
    end
    mock.verify
  end

  def test_model_class
    assert_equal(:playlist, Music::PlaylistRepository.model())
  end

end
