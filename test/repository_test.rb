require_relative 'test_helper'
require_relative '../lib/Music/repository/repository'
require 'singleton'

Cat = Struct.new(:id, :name)
Dog = Struct.new(:id, :name)

describe 'Repository' do

  before do
    @r = Singleton.__init__(Music::Repository).instance
  end

  def test_repository_class_exists
    assert Music::Repository.class
  end

  def test_missing_class_name
    assert_nil(@r.find_by_class(class_name: 'fish'))
    #assert_raises KeyError do
    #  @r.find_by_class(class_name: 'fish')
    #end
  end

  def test_add_new_class_name
    @r.save(class_name: 'dogs', entity: Dog.new('1', 'fido'))
    refute_empty @r.find_by_class(class_name: 'dogs')
  end

  def test_find_by_existing_id
    @r.save(class_name: 'cats', entity: Cat.new('1', 'chester'))
    result = @r.find_by_id(class_name: 'cats', id: '1')
    assert_equal('1', result.id)
    assert_equal('chester', result.name)
  end

  def test_find_by_nonexisting_id
    assert_nil(@r.find_by_id(class_name: 'cats', id: '2'))
    #assert_raises KeyError do
    #  @r.find_by_id(class_name: 'cats', id: '2')
    #end
  end

  def test_delete_by_id
    c = Cat.new('1', 'chester')
    @r.save(class_name:'cats', entity: c)
    @r.delete_by_id(class_name: 'cats', id: c.id)
    assert_nil(@r.find_by_id(class_name: 'cats', id: c.id))
    #assert_raises KeyError do
    #  @r.find_by_id(class_name: 'cats', id: c.id)
    #end
  end

  def test_delete_by_nonexisting_id
    c = Cat.new('1', 'chester')
    @r.save(class_name:'cats', entity: c)
    assert_nil(@r.find_by_id(class_name: 'cats', id: '2'))
    #assert_raises KeyError do
    #  @r.find_by_id(class_name: 'cats', id: '2')
    #end
  end

end
