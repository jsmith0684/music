require_relative 'test_helper'
require_relative '../lib/Music/model/playlist'

describe 'Playlist' do
  before do
    @playlist = Music::Playlist.new(id: '1', user_id: '1', song_ids: ['1', '2'])
    @playlist_json = '{"id": "2", "user_id": "2", "song_ids": ["3", "4"]}'
  end

  def test_playlist_class_exists
    assert(Music::Playlist.class)
  end

  def test_playlist_properties
    assert_equal('1', @playlist.id)
    assert_equal('1', @playlist.user_id)
    assert_equal(['1', '2'], @playlist.song_ids)
  end

  def test_playlist_to_json
    u = @playlist.to_json()
    assert_kind_of(String, u)
  end
end
