require_relative 'test_helper'
require_relative '../lib/Music/utility/file_reader'

describe 'FileReader' do

  def test_filereader_class_exists
    assert FileReader.class
  end

  def test_parse_file
    json = '{"key1":"value1","key2":"value2"}'
    mock = Minitest::Mock.new()
    mock.expect(:parse, json, [Hash])
    f = FileReader.new(file_path:'/tmp/myfile', formatter: mock)
    File.stub(:read, json) do
      f.parse()
    end
    mock.verify
  end

end
