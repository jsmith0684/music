require_relative 'test_helper'
require_relative '../lib/Music/repository/user_repository'

class Validator
  def self.validate(playlist:, result:true)
    result
  end
end
class User
  def initialize(obj)
  end
end

describe 'UserRepository' do
  def test_userrepository_class_exists
    assert Music::UserRepository.class
  end

  def test_model_class
    assert_equal(:user, Music::UserRepository.model())
  end

  def test_save_exit_invalid_user
    user = {:id => '1'}
    mock = MiniTest::Mock.new()
    mock.expect(:validate, false, [Hash])
    original = $stdout.clone()
    $stdout.reopen(File.new('/dev/null', 'w'))
    Validator.stub(:validate, mock) do
      assert_raises SystemExit do
        Music::UserRepository.save(user: user, model: User, validator: mock)
      end
    end
    $stdout.reopen(original)
    mock.verify
  end

  def test_save_valid_user
    user = {:id => '1'}
    mock = MiniTest::Mock.new()
    mock.expect(:validate, true, [Hash])
    mock.expect(:call, nil, [Hash])
    Music::Repository.instance.stub(:save, mock) do
      assert_nil(Music::UserRepository.save(user: user, model: User, validator: mock))
    end
    mock.verify
  end
end
