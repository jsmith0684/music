require_relative 'test_helper'
require_relative '../lib/Music/model/song'

describe 'Song' do
  before do
    @song = Music::Song.new(id: '1', artist: 'the beatles', title: 'while my guitar gently weeps')
    @song_json = '{"id": "2", "artist": "cream", "title": "politician"}'
  end

  def test_song_class_exists
    assert Music::Song.class
  end

  def test_song_properties
    assert_equal('1', @song.id)
    assert_equal('the beatles', @song.artist)
    assert_equal('while my guitar gently weeps', @song.title)
  end

  def test_song_to_json
    s = @song.to_json()
    assert_kind_of(String, s)
  end
end
