require_relative 'test_helper'
require_relative '../lib/Music/utility/file_writer'

class Format
  def format(content:)
    content
  end
end

describe 'FileWriter' do

  def test_filewriter_class_exists
    assert FileWriter.class
  end

  def test_write_file
    content = '{"key1":"value1", "key2":"value2"}'
    mock = Minitest::Mock.new()
    mock.expect(:write, content, [String])
    formatter = Format.new()
    writer = FileWriter.new(formatter: formatter, content: content)
    File.stub(:open, true, mock) do
      File.open('/dev/null', 'w') do |f|
        writer.generate(destination_path:'/dev/null')
      end
    end
    mock.verify
  end

end
