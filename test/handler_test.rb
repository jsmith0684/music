require_relative 'test_helper'
require_relative '../lib/Music/handler/handler'

class Repo
  def self.get_playlists()
    {}
  end

  def self.get_users()
    {}
  end

  def self.get_songs()
    {}
  end

  def self.save(playlist:nil, user:nil, song:nil)
  end

  def self.delete_by_id(id:)
  end
end

describe 'Handler' do

  before do
    @h = Music::Handler.new(songs: Repo, users: Repo, playlists: Repo)
  end

  def test_handler_class_exists
    assert Music::Handler.class
  end

  def test_apply_changes_repository
    changes = {:playlists => {:update => [], :delete => [], :create => []}}
    assert @h.apply_changes_repository(changes: changes)
  end

  def test_apply_changes_repository_without_playlists
    changes = {}
    original = $stdout.clone()
    $stdout.reopen(File.new('/dev/null', 'w'))
    assert_raises SystemExit do
      @h.apply_changes_repository(changes: changes)
    end
    $stdout.reopen(original)
  end

  def test_generate_output
    assert @h.generate_output()
  end

  def test_hydrate_repository
    source = {:playlists => [], :users => [], :songs => []}
    assert @h.hydrate_repository(source: source)
  end

end
