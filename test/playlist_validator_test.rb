require_relative 'test_helper'
require_relative '../lib/Music/validator/playlist_validator'

class UserRepository
  def self.find_by_id(id:)
    h = {'1' =>{:id=>'1',:name =>'bob'}, '2' =>{:id=>'2',:name=>'jane'}}
    h[id]
  end
end

class SongRepository
  def self.find_by_id(id:)
    h = {'1' =>{:id=>'1',:artist=>'u2',:title=>'who'},'2'=> {:id=>'2',:artist=>'bob dylan', :title=>'like a rolling stone'}}
    h[id]
  end
end

describe 'PlaylistValidator' do

  def test_playlist_validator_class_exists
    assert Music::PlaylistValidator.class
  end

  def test_empty_playlist_object
    refute Music::PlaylistValidator.validate(playlist:{},users_repo:UserRepository, songs_repo: SongRepository)
  end

  def test_nonexisting_user_id
    refute Music::PlaylistValidator.validate(playlist:{:user_id=>'10'},users_repo:UserRepository, songs_repo: SongRepository)
  end

  def test_nonexisting_song_id
    refute Music::PlaylistValidator.validate(playlist:{:user_id=>'2',:song_ids=>['1', '2', '3']},users_repo:UserRepository, songs_repo: SongRepository)
  end

  def test_valid_playlist
    assert Music::PlaylistValidator.validate(playlist:{:id =>'2',:user_id=>'2',:song_ids=>['1', '2']},users_repo:UserRepository, songs_repo: SongRepository)
  end

  def test_no_song_ids_key
    refute Music::PlaylistValidator.validate(playlist:{:user_id=>'2',:id =>'1'},users_repo:UserRepository, songs_repo: SongRepository)
  end

  def test_no_user_id_key
    refute Music::PlaylistValidator.validate(playlist:{:id =>'1',:song_ids=>['1', '2']},users_repo:UserRepository, songs_repo: SongRepository)
  end

  def test_no_id_key
    refute Music::PlaylistValidator.validate(playlist:{:user_id=>'2',:song_ids=>['1', '2']},users_repo:UserRepository, songs_repo: SongRepository)
  end

end
