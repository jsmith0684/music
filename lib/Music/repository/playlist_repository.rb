require 'Music/repository/repository'
require 'Music/model/playlist'
require 'Music/validator/playlist_validator'
require 'Music/utility/logging'

module Music
  class PlaylistRepository
    include Logging

    def self.model()
      :playlist
    end

    def self.save(playlist:, model: Playlist, validator: PlaylistValidator)
      if validator.validate(playlist: playlist)
        p = model.new(playlist)
        Repository.instance.save(class_name: model(), entity: p)
      else
        # TODO: log specific validation failure
        logger.error("ERROR: Playlist with id=#{playlist[:id] || 'MISSING'} is invalid. Output will not be written.")
        exit(1)
      end
    end

    def self.find_by_id(id:)
      Repository.instance.find_by_id(class_name: model(), id: id)
    end

    def self.delete_by_id(id:)
      Repository.instance.delete_by_id(class_name: model(), id: id)
    end

    def self.get_playlists()
      Repository.instance.find_by_class(class_name: model())
    end
  end
end
