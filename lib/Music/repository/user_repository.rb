require 'Music/repository/repository'
require 'Music/model/user'
require 'Music/validator/user_validator'
require 'Music/utility/logging'

module Music
  class UserRepository
    include Logging

    def self.model()
      :user
    end

    def self.save(user:, model: User, validator: UserValidator)
      if validator.validate(user: user)
        u = model.new(id: user[:id], name: user[:name])
        Repository.instance.save(class_name: model(), entity: u)
      else
        # TODO: log specific validation failure
        logger.error("ERROR: User with id=#{user[:id] || 'MISSING'} is invalid. Output will not be written.")
        exit(1)
      end
    end

    def self.find_by_id(id:)
      Repository.instance.find_by_id(class_name: model(), id: id)
    end

    def self.delete_by_id(id:)
      Repository.instance.delete_by_id(class_name: model(), id: id)
    end

    def self.get_users()
      Repository.instance.find_by_class(class_name: model())
    end
  end
end
