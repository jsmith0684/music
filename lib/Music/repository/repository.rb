require 'json'
require 'singleton'

module Music
  class Repository
    include Singleton

    attr_reader :storage

    def initialize(storage: Hash.new())
      @storage = storage
    end

    def save(class_name:, entity:)
      @storage[class_name] ||= {}
      @storage[class_name][entity.id] = entity
    end

    def find_by_id(class_name:, id:)
      if @storage.key?(class_name)
        @storage[class_name][id]
      else
        nil
      end
    end

    def find_by_class(class_name:)
      @storage[class_name]
    end

    def delete_by_id(class_name:, id:)
      if @storage.key?(class_name)
        @storage.fetch(class_name).delete(id)
      end
    end

  end
end
