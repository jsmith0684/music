require 'Music/repository/repository'
require 'Music/model/song'
require 'Music/validator/song_validator'
require 'Music/utility/logging'

module Music
  class SongRepository
    include Logging

    def self.model()
      :song
    end

    def self.save(song:, model: Song, validator: SongValidator)
      if validator.validate(song: song)
        s = model.new(song)
        Repository.instance.save(class_name: model(), entity: s)
      else
        # TODO: log specific validation failure
        logger.error("ERROR: Song with id=#{song[:id] || 'MISSING'} is invalid. Output will not be written.")
        exit(1)
      end
    end

    def self.find_by_id(id:)
      Repository.instance.find_by_id(class_name: model(), id: id)
    end

    def self.delete_by_id(id:)
      Repository.instance.delete_by_id(class_name: model(), id: id)
    end

    def self.get_songs()
      Repository.instance.find_by_class(class_name: model())
    end
  end
end
