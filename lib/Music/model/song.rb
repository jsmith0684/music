require 'json'

module Music
  class Song
    attr_accessor :artist, :title
    attr_reader :id

    def initialize(id:, artist:, title:)
      @id = id
      @artist = artist
      @title = title
    end

    # This logic can be added to a serializers class
    def to_json(*args)
      {'id' => @id, 'artist' => @artist, 'title' => @title}.to_json(*args)
    end
  end
end
