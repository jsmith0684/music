require 'json'

module Music
  class Playlist
    attr_accessor :user_id, :song_ids
    attr_reader :id

    def initialize(id:,user_id:, song_ids:)
      @id = id
      @user_id = user_id
      @song_ids = song_ids
    end

    # This logic can be added to a serializers class
    def to_json(*args)
      {'id' => @id, 'user_id' => @user_id, 'song_ids' => @song_ids}.to_json(*args)
    end
  end
end
