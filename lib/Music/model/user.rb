require 'json'

module Music
  class User
    attr_accessor :name
    attr_reader :id

    def initialize(id:, name:)
      @id = id
      @name = name
    end

    # This logic can be added to a serializers class
    def to_json(*args)
      {'id' => @id, 'name' => @name}.to_json(*args)
    end
  end
end
