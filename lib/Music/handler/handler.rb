require 'Music/repository/user_repository'
require 'Music/repository/song_repository'
require 'Music/repository/playlist_repository'
require 'Music/utility/logging'

module Music
  class Handler
    include Logging

    attr_reader :users, :songs, :playlists

    def initialize(users: UserRepository, songs: SongRepository, playlists: PlaylistRepository)
      @users = users
      @songs = songs
      @playlists = playlists
    end

    def hydrate_repository(source:)
      # playlist validation assumes song ids and user id exists
      # therefore users and songs must be saved prior to playlists
      keys = source.keys().sort().reverse()
      keys.each do |table|
        source[table].each do |obj|
          case table
          when :playlists
            playlists.save(playlist: obj)
          when :users
            users.save(user: obj)
          when :songs
            songs.save(song: obj)
          end
        end
      end
    end

    def apply_changes_repository(changes:)
      changes.fetch(:playlists).keys().each do |action|
        changes[:playlists][action].each do |obj|
          case action
          when :create, :update
            playlists.save(playlist: obj)
          when :delete
            playlists.delete_by_id(id: obj[:id])
          end
        end
      end
    rescue KeyError => e
      logger.error("ERROR: no playlists found - #{e.message}")
      exit(1)
    end

    def generate_output()
      {
        users: users.get_users().values(),
        playlists: playlists.get_playlists().values(),
        songs: songs.get_songs().values()
      }
    end
  end
end
