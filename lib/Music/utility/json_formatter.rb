require 'json'
require 'Music/utility/logging'

class JSONFormatter
  include Logging

  def format(content:)
    JSON.pretty_generate(content)
  rescue TypeError => e
    logger.error("ERROR: #{e.message}")
    exit(1)
  end

  def parse(content:, symbolize_names: true)
    JSON.parse(content, symbolize_names: symbolize_names)
  rescue JSON::ParserError => e
    logger.error("ERROR: Could not parse input - #{e.message}")
    exit(1)
  end
end
