require 'logger'

module Logging
  def self.included(base)
    class << base
      def logger()
        @logger ||= Logger.new(STDOUT)
      end

      def logger=(logger)
        @logger = logger
      end
    end
  end

  def logger()
    self.class.logger
  end
end
