require 'Music/utility/logging'

class FileReader
  include Logging
  attr_reader :formatter, :file_path

  def initialize(formatter: JSONFormatter.new, file_path:)
    @formatter = formatter
    @file_path = file_path
  end

  def parse(reader: File)
    file = reader.read(file_path)
    formatter.parse(content: file)
  rescue StandardError => e
    logger.error("ERROR: #{e.message}")
    exit(1)
  end
end
