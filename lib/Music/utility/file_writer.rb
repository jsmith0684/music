require 'Music/utility/logging'

class FileWriter
  include Logging
  attr_reader :content, :formatter

  def initialize(formatter: JSONFormatter.new, content:)
    @formatter = formatter
    @content = content
  end

  def generate(destination_path:, writer: File)
    writer.open(destination_path, 'w') do |f|
      f.write(formatter.format(content: content))
    end
  rescue StandardError => e
    logger.error("ERROR: #{e.message}")
    exit(1)
  end
end
