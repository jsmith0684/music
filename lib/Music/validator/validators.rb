module Validators
  def self.valid_string?(object, key)
    return false unless object.key?(key)
    return false unless object[key].is_a?(String)
    return false if object[key].empty?
    return true
  end

  def self.valid_array?(object, key)
    return false unless object.key?(key)
    return false unless object[key].is_a?(Array)
    return true
  end
end
