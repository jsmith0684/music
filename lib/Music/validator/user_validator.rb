require 'Music/validator/validators'

module Music
  class UserValidator
    include Validators

    def self.validate(user:)
      return false if user.empty?
      return false unless Validators.valid_string?(user, :id)
      return false unless Validators.valid_string?(user, :name)
      return true
    end
  end
end
