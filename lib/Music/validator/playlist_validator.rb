require 'Music/repository/user_repository'
require 'Music/repository/playlist_repository'
require 'Music/validator/validators'
module Music
  class PlaylistValidator
    include Validators

    def self.validate(playlist:, users_repo: UserRepository, songs_repo: SongRepository)
      return false if playlist.empty?
      return false unless Validators.valid_string?(playlist, :id)
      return false unless Validators.valid_string?(playlist, :user_id)
      return false unless Validators.valid_array?(playlist, :song_ids)
      return false if users_repo.find_by_id(id: playlist[:user_id]).nil?
      return false if playlist[:song_ids].any? { |id| songs_repo.find_by_id(id: id).nil? }
      return true
    end
  end
end
