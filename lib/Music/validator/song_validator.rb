require 'Music/validator/validators'

module Music
  class SongValidator
    include Validators

    def self.validate(song:)
      return false if song.empty?
      return false unless Validators.valid_string?(song, :id)
      return false unless Validators.valid_string?(song, :title)
      return false unless Validators.valid_string?(song, :artist)
      return true
    end
  end
end
