require 'optparse'
require 'Music/version'
require 'Music/handler/handler'
require 'Music/utility/json_formatter'
require 'Music/utility/file_writer'
require 'Music/utility/file_reader'

module Music
  class Command
    def self.start()
      opt_parser = OptionParser.new do |opt|
        opt.banner = 'Usage: Music mixtapes.json changes.json output.json'

        opt.on('-v', '--version', 'Prints version number') do
          puts Music::VERSION
          exit(0)
        end

        opt.on_tail('-h', '--help', 'Prints help message') do
          puts opt
          exit(0)
        end
      end

      opt_parser.parse!()

      unless ARGV.length == 3
        puts opt_parser
        exit(1)
      end

      source_path = ARGV.shift()
      changes_path = ARGV.shift()
      destination_path = ARGV.shift()

      return {
        :source_path => source_path,
        :changes_path => changes_path,
        :dest_path => destination_path
      }
    end

    def self.process_input(source_path:, changes_path:, dest_path:)
      source = FileReader.new(formatter: JSONFormatter.new, file_path: source_path).parse()
      changes = FileReader.new(formatter: JSONFormatter.new, file_path: changes_path).parse()
      handler = Music::Handler.new()
      handler.hydrate_repository(source: source)
      handler.apply_changes_repository(changes: changes)
      result = handler.generate_output()
      FileWriter.new(formatter: JSONFormatter.new(), content: result).generate(destination_path: dest_path)
    end
  end
end
